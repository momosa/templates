## Overview

A brief description of the task

## Criteria

Criteria for selecting the architecture

## Additional info

Some more info that may help complete the task

## Success criteria

- [ ] The team has reviewed the architecture
- [ ] The selection and rationale have been documented
- [ ] There are tickets resulting from this work
