## Overview

A brief description of the task

## Criteria

When a choice is involved (e.g., choosing between two ways of doing something),
what are the criteria for making the choice?
E.g., choosing the right tool to accomplish a task.

## Additional info

Some more info that may help complete the task

## Success criteria

- [ ] The team has reviewed the choice
- [ ] The selection and rationale have been documented
- [ ] We have created tickets resulting from this investigation
