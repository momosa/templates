## Overview

A brief description of the task

## Prerequisites

Link to card
Link to card

## Description and Rationale

Anyone on the team should be able to understand what this ticket is about from this section.

Provide a high-level description of what this work aims to accomplish.

How does this work contribute to our current goals?

## Success criteria

- [ ] A concise, high-level, declarative sentence describing the state of the universe when this task is done
- [ ] A concise, high-level, declarative sentence describing the state of the universe when this task is done

## Additional info

Do you have any ideas about how the feature should be implemented?

Please provide details and/or sketches.

Ideally, make it straightforward for anyone to take on.

To-do's should typically go here, not in success criteria.

## Testing/validation notes

Notes about tests to implement or
methods of manual validation.
