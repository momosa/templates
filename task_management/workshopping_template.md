_Requires that the corresponding planning task has been completed._

1. Add details to tasks in triage.
1. Add/review the timebox value for each task.
1. Add tasks to the **To-do** column.

