Create upcoming tasks and determine labels:

- **Milestone**: The milestone corresponding to the task. There should be cards like `milestone: WTP 1.0`
- **Role**: The type of skills required for the work (engineering, administration, etc.)
- **Mode**: The kind of work it is: implementing something you know how to do or investigating how to do something
- **Priority**: How important is the work for the project and in relation to other tasks.

Add a timebox value for the maximum amount of time that should be spent on the work. The timebox may be changed during the workshopping phase.

No need to add too many details yet. Save that for the workshopping step.
