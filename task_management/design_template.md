## Overview

A brief description of the task

## Criteria

Criteria for selecting the design,
which may include relevant workflows,
accessibility considerations,
etc.

## Additional info

Some more info that may help complete the task

## Success criteria

- [ ] The team has reviewed the design
- [ ] The selection and rationale have been documented
- [ ] There are tickets resulting from this design
